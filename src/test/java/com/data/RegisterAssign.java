package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterAssign {

	public static void main(String[] args) throws BiffException, IOException {
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
File file = new File("/home/manobala/Pictures/DataDriven/datas.xls");
		
		FileInputStream fis = new FileInputStream(file);
		
		Workbook book = Workbook.getWorkbook(fis);
		
		Sheet sht = book.getSheet("Sheet2");
		
		int rows = sht.getRows();
		int column = sht.getColumns();
		
		for (int i = 1; i < rows; i++) {
			
			String firstname = sht.getCell(0,i).getContents();
			String lastname = sht.getCell(1,i).getContents();
			String email = sht.getCell(2,i).getContents();
			String password = sht.getCell(3,i).getContents();
			String confirmpassword = sht.getCell(4,i).getContents();
			
			driver.findElement(By.xpath("//a[@class='ico-register']")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(confirmpassword);
			
			
	}
}
}