package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class FetchingValues {

	public static void main(String[] args) throws BiffException, IOException {
		
		File file = new File("/home/manobala/Pictures/DataDriven/datas.xls");
		
		FileInputStream fis = new FileInputStream(file);
		
		Workbook book = Workbook.getWorkbook(fis);
		
		Sheet sht = book.getSheet("Sheet1");
		
		String celldata = sht.getCell(0,0).getContents();
		
		int rows = sht.getRows();
		int column = sht.getColumns();
		
		for (int i = 0; i < rows; i++) {
			
			for (int j = 0; j < column; j++) {
				
				celldata = sht.getCell(j,i).getContents();
				
				System.out.println(celldata);
				
			}
		}
		
	}
}
