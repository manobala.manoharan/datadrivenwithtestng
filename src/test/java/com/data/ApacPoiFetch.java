package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;




public class ApacPoiFetch {

	public static void main(String[] args) throws IOException {
		
		File file = new File("/home/manobala/Music/TestData/data.xlsx");
		
		FileInputStream fis = new FileInputStream(file);
		
		XSSFWorkbook book = new XSSFWorkbook(fis);
		
		XSSFSheet sheet = book.getSheetAt(0);
		
		int rows = sheet.getLastRowNum();
		
		for (int i = 0; i <= rows; i++) {
			int columns = sheet.getRow(i).getLastCellNum();
			for (int j = 0; j < columns; j++) {
				String cellvalue = sheet.getRow(i).getCell(j).getStringCellValue();
				System.out.println(cellvalue);
			}
		}
		
		
}
}