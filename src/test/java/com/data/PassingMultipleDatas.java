package com.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingMultipleDatas {

	public static void main(String[] args) throws BiffException, IOException {

WebDriver driver = new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
File file = new File("/home/manobala/Pictures/DataDriven/datas.xls");
		
		FileInputStream fis = new FileInputStream(file);
		
		Workbook book = Workbook.getWorkbook(fis);
		
		Sheet sht = book.getSheet("Sheet1");
		
		int rows = sht.getRows();
		int column = sht.getColumns();
		
		for (int i = 1; i < rows; i++) {
			
			String username = sht.getCell(0,i).getContents();
			String password = sht.getCell(1,i).getContents();
			
		
		WebElement login = driver.findElement(By.xpath("//a[@class='ico-login']"));
		login.click();
		
		WebElement email = driver.findElement(By.id("Email"));
		email.sendKeys(username);
		
		WebElement pass = driver.findElement(By.id("Password"));
		pass.sendKeys(password);
		
		WebElement clicklogin = driver.findElement(By.xpath("//input[@value='Log in']"));
		clicklogin.click();
		
		WebElement logout = driver.findElement(By.xpath("//a[@class='ico-logout']"));
		logout.click();
  }
	}
}	

